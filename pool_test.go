package pools_test

import (
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"codeberg.org/gruf/go-pools"
)

type badPool struct {
	New   func() interface{}
	Evict func(interface{})
	pool  []interface{}
	mu    sync.Mutex
}

func (p *badPool) startEvicts() {
	for {
		time.Sleep(time.Second)
		p.mu.Lock()
		if len(p.pool) > 1 {
			l := len(p.pool) - 1
			v := p.pool[l]
			p.pool = p.pool[:l-1]
			p.Evict(v)
		}
		p.mu.Unlock()
	}
}

func (p *badPool) Get() interface{} {
	p.mu.Lock()
	if len(p.pool) < 1 {
		p.mu.Unlock()
		return p.New()
	}
	l := len(p.pool) - 1
	v := p.pool[l]
	p.pool = p.pool[:l]
	p.mu.Unlock()
	return v
}

func (p *badPool) Put(v interface{}) {
	if v == nil {
		return
	}
	p.mu.Lock()
	p.pool = append(p.pool, v)
	p.mu.Unlock()
}

type testItem struct {
	i int32
}

func (i *testItem) check() bool {
	defer atomic.StoreInt32(&i.i, 0)
	return atomic.CompareAndSwapInt32(&i.i, 0, 1)
}

func BenchmarkPool(b *testing.B) {
	p := &pools.Pool{}

	p.New = func() interface{} {
		return &testItem{}
	}

	p.Evict = func(i interface{}) {
		if !i.(*testItem).check() {
			panic("in use during eviction")
		}
	}

	b.ResetTimer()
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			i := p.Get().(*testItem)
			if !i.check() {
				panic("in use after get")
			}
			p.Put(i)
		}
	})
}

func BenchmarkSyncPool(b *testing.B) {
	p := sync.Pool{}

	p.New = func() interface{} {
		return &testItem{}
	}

	b.ResetTimer()
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			i := p.Get().(*testItem)
			if !i.check() {
				panic("in use after get")
			}
			p.Put(i)
		}
	})
}

func BenchmarkBadPool(b *testing.B) {
	p := badPool{}
	go p.startEvicts()

	p.New = func() interface{} {
		return &testItem{}
	}

	p.Evict = func(i interface{}) {
		if !i.(*testItem).check() {
			panic("in use during eviction")
		}
	}

	b.ResetTimer()
	b.ReportAllocs()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			i := p.Get().(*testItem)
			if !i.check() {
				panic("in use after get")
			}
			p.Put(i)
		}
	})
}
