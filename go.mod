module codeberg.org/gruf/go-pools

go 1.16

require (
	codeberg.org/gruf/go-bytes v1.0.2 // indirect
	codeberg.org/gruf/go-byteutil v1.0.0
	codeberg.org/gruf/go-fastpath v1.0.1
	codeberg.org/gruf/go-hashenc v1.0.2
)
